#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import copy

def can_move(panel, pos, move):
  x = pos[0] + move[0]
  y = pos[1] + move[1]
  return not(x < 0 or x > 4 or y < 0 or y > 4 or panel[x][y] != 0)

def solve(panel, start):
  count = 0
  q = [(panel, start)]
  while len(q) != 0:
    table, pos = q.pop(0)
    if pos[0] == 4 and pos[1] == 4:
      count += 1
      continue
    table[pos[0]][pos[1]] = 1

    for move in [(0, -1), (0, 1), (-1, 0), (1, 0)]:
      if can_move(table, pos, move):
        tmp = copy.deepcopy(table)
        q.append((tmp, (pos[0] + move[0], pos[1] + move[1])))

  return count

def main():
  panel = [
           [0,-1, 0,-1, 0],
           [0, 0, 0, 0,-1],
           [0, 0, 0, 0, 0],
           [0,-1, 0, 0, 0],
           [0,-1, 0, 0, 0],
          ]
  answer = solve(panel, (0, 0))
  print('answer: {0}'.format(answer))

if __name__ == '__main__':
  main()

