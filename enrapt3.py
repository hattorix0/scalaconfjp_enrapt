#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import sys

def solve(graph, start, goal):
  table = dict([(k, [sys.maxsize, sys.maxsize]) for k, v in graph.items()])
  table[goal] = [sys.maxsize, sys.maxsize]
  table[start][0] = table[start][1] = 0

  q = [start]
  while len(q) != 0:
    pos = q.pop(0)
    if pos == goal:
      continue

    for nxt in graph[pos]:
      name = nxt['name']
      table[name][0] = min(table[name][0],
                           table[pos][0] + nxt['distance'])
      table[name][1] = min(table[name][1],
                           table[pos][1] + nxt['rate'])
      if name in q:
        i = q.index(name)
        q[i], q[len(q)-1] = q[len(q)-1], q[i]
      else:
        q.append(name)

  return table

def load_input():
  # from,to,距離,運賃
  xx = """たかし家前,御湯ノ水,2,160
御湯ノ水,夏葉原,15,140
御湯ノ水,霜ヶ関,21,160
夏葉原,小森,18,150
霜ヶ関,小森,14,140
霜ヶ関,器川,2,30
器川,小森,13,120
器川,夏葉原,14,140
小森,市場前,5,140"""

  dic = {}
  for line in xx.splitlines():
    columns = line.split(',')
    route = {
      'name'    : columns[1].strip(),
      'distance': int(columns[2]),
      'rate'    : int(columns[3]),
      }

    if columns[0] not in dic:
      dic[columns[0]] = [route]
    else:
      dic[columns[0]].append(route)
  return dic

def main():
  graph = load_input()
  table = solve(graph, "たかし家前", "市場前")
  for k, v in table.items():
    print(k, v)
  print('answer: {0}km, {1}yen'.format(table["市場前"][0], table["市場前"][1]))

if __name__ == '__main__':
  main()

