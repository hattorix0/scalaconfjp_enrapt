#!/usr/bin/env python3
# -*- coding: utf-8 -*-

def can_move(table, pos, move):
  x = pos[0] + move[0]
  y = pos[1] + move[1]
  return not(x < 0 or x >= len(table[0]) or y < 0 or y >= len(table))

def get_value(table, pos, move=(0, 0)):
  x = pos[0] + move[0]
  y = pos[1] + move[1]
  if x < 0 or x >= len(table[0]) or y < 0 or y >= len(table):
    return 0
  return table[x][y]

def get_max(table, panel, pos):
  v0 = get_value(table, pos, (0, -1))
  v1 = get_value(table, pos, (-1, 0))
  return panel[pos[0]][pos[1]] + max(v0, v1)

def solve(panel, start):
  table = [[0] * len(panel[_]) for _ in range(len(panel))]
  q = [start]
  while len(q) != 0:
    pos = q.pop(0)
    if get_value(table, pos) > 0:
      continue
    table[pos[0]][pos[1]] = get_max(table, panel, pos)

    if can_move(panel, pos, (0, 1)):
      q.append((pos[0], pos[1]+1)) # 下
    if can_move(panel, pos, (1, 0)):
      q.append((pos[0]+1, pos[1])) # 右

  row = table[len(table) - 1]
  return row[len(row) - 1], table

def main():
  panel = (
           (0, 5, 8, 3, 5, 2, 9, 5),
           (7, 5, 9, 3, 8, 2, 7, 4),
           (3, 7, 5, 6, 4, 4, 2, 1),
           (0, 5, 7, 3, 9, 7, 8, 4),
           (3, 6, 4, 6, 0, 6, 3, 8),
           (2, 9, 6, 8, 6, 8, 2, 1),
           (1, 8, 5, 9, 8, 2, 3, 0),
           (9, 7, 7, 3, 9, 2, 1, 4),
          )
  answer, table = solve(panel, (0, 0))
  for row in table:
    print(row)
  print('answer: {0}'.format(answer))

if __name__ == '__main__':
  main()

